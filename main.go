package main

import (
	"activewear-woo/app/sync"
	"fmt"
	"github.com/robfig/cron/v3"
	"time"
)

var Queue = make(map[string]string)

func main()  {
	fmt.Println("started")

	cronJob := cron.New()
	cronJob.AddFunc("@every 6h", func() {
		if len(Queue) == 0 {
			Queue["stock"] = "stock"
			sync.StockSync()
			delete(Queue, "stock")
		}
	})

	cronJob.AddFunc("0 0 0 5 1 *", func() {
		for {
			if len(Queue) == 0 {
				Queue["sync"] = "sync"
				sync.StartSync()
				delete(Queue, "sync")
				break
			} else {
				time.Sleep(1*time.Hour)
			}
		}
	})

	cronJob.AddFunc("0 0 0 5 7 *", func() {
		for {
			if len(Queue) == 0 {
				Queue["sync"] = "sync"
				sync.StartSync()
				delete(Queue, "sync")
				break
			} else {
				time.Sleep(1*time.Hour)
			}
		}
	})
	cronJob.Start()

	for {
		time.Sleep(time.Second)
	}
}