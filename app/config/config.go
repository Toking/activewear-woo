package config

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
)

var Configuration = NewConfig("config.yml")

type Config struct {
	Settings  struct {
		AwUsername        string `yaml:"awUsername"`
		AwKey             string `yaml:"awKey"`
		ConsumerKey     string `yaml:"consumerKey"`
		ConsumerSecret string `yaml:"consumerSecret"`
		Url 		   string `yaml:"url"`
	} `yaml:"settings"`
}

func NewConfig(file string) *Config {
	var cfg Config
	err := readFile(file, &cfg)
	if err != nil {
		fmt.Printf("Unable to create conf: %v", err)
		os.Exit(2)
	}
	return &cfg
}

func readFile(file string, cfg *Config) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	return err
}
