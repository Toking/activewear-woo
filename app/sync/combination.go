package sync

import (
	"activewear-woo/app/activewear"
	wooApi "activewear-woo/app/woo"
	"fmt"
	"strconv"
	"strings"
)

func CreateWooProductsFromErply(shouldCreate map[int]activewear.Style, callerMethod string, awProducts []activewear.Product, variationsMap map[string][]activewear.Product, awCategories []activewear.Category, wooCategories []wooApi.Category, wooProducts []wooApi.Product, client wooApi.WooClient) ([]wooApi.Product, error) {
	wooAttributes, err := client.GetAllAttributes()
	if err != nil {
		return nil, err
	}

	var wooProductsToCreate []wooApi.Product
	for _, i := range shouldCreate {
		wp := wooApi.Product{
			Featured:    false,
			ManageStock: false,
			Type:  wooApi.WooVariableType,
			Status: "publish",
			Sku: i.Partnumber,
		}

		wooProductID := GetWooProductIDByAWID(wooProducts, strconv.Itoa(i.Styleid))
		if wooProductID != 0 {
			wp.ID = wooProductID
		}
		wp.Name = i.Brandname + " " + i.Title
		wp.Description = i.Description

		var variations []activewear.Product
		if val, ok := variationsMap[strconv.Itoa(i.Styleid)]; ok {
			variations = val
		}
		var attributes []wooApi.Attribute
		for _, v := range variations {
			attributes = StructureAttributes(wooAttributes, wooApi.Color, v.Colorname, attributes)
			attributes = StructureAttributes(wooAttributes, wooApi.Size, v.Sizename, attributes)
		}
		wp.Attributes = attributes

		wooProduct := GetWooProductByAWID(wooProducts, strconv.Itoa(i.Styleid))
		if i.Brandname != ""{
			id := 0
			for _, wa := range wooAttributes {
				if wooApi.BrandAttribute == wa.Name {
					id = wa.ID
				}
			}

			var options []string
			options = append(options, i.Brandname)
			attribute := wooApi.Attribute{
				ID:        id,
				Name:      wooApi.BrandAttribute,
				Visible:   true,
				Variation: false,
				Options:   options,
			}
			wp.Attributes = append(wp.Attributes, attribute)
		} else if wooProduct != nil && ContainsAttribute(wooProduct.Attributes, wooApi.BrandAttribute) {
			attr := GetAttributeByName(wooProduct.Attributes, wooApi.BrandAttribute)
			if attr != nil {
				attribute := wooApi.Attribute{
					ID:        attr.ID,
					Name:      wooApi.BrandAttribute,
					Visible:   true,
					Variation: false,
					Options:   attr.Options,
				}
				wp.Attributes = append(wp.Attributes, attribute)
			}
		}

		for k, wa := range wp.Attributes {
			if wa.ID == 0 {
				attribute := &wooApi.Attribute{
					Name: wa.Name,
				}
				res, err := client.AddAttribute(attribute)
				if err != nil {
					return nil, err
				}
				if res != nil {
					wooAttributes = append(wooAttributes, *res)
					wp.Attributes[k].ID = res.ID
				}
			}
		}

		if i.Styleimage != "" {
			var images []wooApi.Image
			lastSlashIndex := strings.LastIndex(i.Styleimage, "/")
			iName := i.Styleimage[lastSlashIndex + 1 :len(i.Styleimage)-4]
			if wooProduct != nil {
				imageID := GetWoocommerceImageIDByErplyImageName(wooProduct.Images, iName)
				if imageID != 0 {
					image := wooApi.Image{Id: imageID, Name: iName, Alt: iName}
					images = append(images, image)
				} else {
					imageUrl := activewear.CDN + i.Styleimage
					image := wooApi.Image{Src: imageUrl, Name: iName, Alt: iName}
					images = append(images, image)
				}
			} else {
				imageUrl := activewear.CDN + i.Styleimage
				image := wooApi.Image{Src: imageUrl, Name: iName, Alt: iName}
				images = append(images, image)
			}

			wp.Images = images
		}

		if i.Categories != "" {
			var wooCats []wooApi.Category
			categoriesArr := strings.Split(i.Categories, ",")
			for _, cat := range categoriesArr {
				wooCat := WooGetCategoryWithSlug(wooCategories, cat)
				if wooCat == nil {
					continue
				} else {
					wooCats = append(wooCats, wooApi.Category{Id: wooCat.Id, Name: wooCat.Name, Slug: wooCat.Slug})
				}
			}
			wp.Categories = wooCats
		}

		var metadata []wooApi.MetaData
		metadata = append(metadata, wooApi.MetaData{Key: wooApi.ProductID, Value: strconv.Itoa(i.Styleid)})
		wp.MetaData = metadata

		wooProductsToCreate = append(wooProductsToCreate, wp)
	}

	return wooProductsToCreate, nil
}

func StructureAttributes(wooAttributes []wooApi.Attribute, attributeName string, attributeValue string, productAttributes []wooApi.Attribute) []wooApi.Attribute {
	id := 0
	for _, wa := range wooAttributes {
		if strings.ToLower(attributeName) == strings.ToLower(wa.Name) {
			id = wa.ID
		}
	}

	if attributeValue == "" {
		return productAttributes
	}

	if ContainsAttribute(productAttributes, attributeName) {
		productAttributes = CheckAndUpdateAttributeOptions(productAttributes, attributeValue, attributeName)
	} else {
		var options []string
		options = append(options, attributeValue)
		attribute := wooApi.Attribute{
			ID:        id,
			Name:      attributeName,
			Visible:   true,
			Variation: true,
			Options:   options,
		}
		productAttributes = append(productAttributes, attribute)
	}

	return productAttributes
}

func StructureAttributesForVariation(wooAttributes []wooApi.Attribute, attributeName string, attributeValue string, productAttributes []wooApi.VariationAttibute) []wooApi.VariationAttibute {
	id := 0
	for _, wa := range wooAttributes {
		if strings.ToLower(attributeName) == strings.ToLower(wa.Name) {
			id = wa.ID
		}
	}

	if attributeValue == "" {
		return productAttributes
	}

	attribute := wooApi.VariationAttibute{
		ID: id,
		Option: attributeValue,
	}

	productAttributes = append(productAttributes, attribute)
	return productAttributes
}

func CreateWooVariationsFromErply(shouldCreate []activewear.Product, wooClient wooApi.WooClient, style *activewear.Style) ([]wooApi.Variation, error) {
	var wooProductsToCreate []wooApi.Variation
	wooAttributes, err := wooClient.GetAllAttributes()
	if err != nil {
		return nil, err
	}

	for _, i := range shouldCreate {
		wp := wooApi.Variation{
			ManageStock: true,
		}
		stock := i.Qty
		wp.AWID = i.SkuidMaster
		for _, wr := range i.Warehouses {
			if strings.Contains(wr.Warehouseabbr, "DS") || wr.Warehouseabbr == "DS" {
				stock = stock - wr.Qty
			}
		}
		wp.StockQuantity = stock
		wp.Name = style.Brandname + " " + style.Title + " " + i.Colorname + " " + i.Sizename
		wp.StockQuantity = &stock

		var attributes []wooApi.VariationAttibute
		attributes = StructureAttributesForVariation(wooAttributes, wooApi.Color, i.Colorname, attributes)
		attributes = StructureAttributesForVariation(wooAttributes, wooApi.Size, i.Sizename, attributes)
		wp.Attributes = attributes

		wp.RegularPrice = fmt.Sprintf("%2f", i.Pieceprice)
		if i.Saleprice != 0 && i.Pieceprice != i.Saleprice{
			wp.SalePrice = fmt.Sprintf("%2f", i.Saleprice)
		} else {
			wp.SalePrice = ""
		}

		dimesions := &wooApi.WooDimensions{
			Length: fmt.Sprintf("%g", i.Caselength),
			Width:  fmt.Sprintf("%g", i.Casewidth),
			Height: fmt.Sprintf("%g", i.Caseheight),
		}
		wp.Dimensions = dimesions
		wp.Weight = fmt.Sprintf("%g", i.Caseweight)
		wp.Sku = i.Sku

		var metadata []wooApi.MetaData
		metadata = append(metadata, wooApi.MetaData{Key: wooApi.ProductID, Value: strconv.Itoa(i.SkuidMaster)})
		wp.MetaData = metadata

		wooProductsToCreate = append(wooProductsToCreate, wp)
	}

	return wooProductsToCreate, nil
}

func CreateWooVariationsForStock(shouldCreate []activewear.Product) ([]wooApi.VariationForStock, error) {
	var wooProductsToCreate []wooApi.VariationForStock
	for _, i := range shouldCreate {
		wp := wooApi.VariationForStock{}
		stock := i.Qty
		wp.AWID = i.SkuidMaster
		for _, wr := range i.Warehouses {
			if strings.Contains(wr.Warehouseabbr, "DS") || wr.Warehouseabbr == "DS" {
				stock = stock - wr.Qty
			}
		}
		wp.StockQuantity = stock
		wooProductsToCreate = append(wooProductsToCreate, wp)
	}

	return wooProductsToCreate, nil
}