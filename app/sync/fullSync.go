package sync

import (
	"activewear-woo/app/activewear"
	"activewear-woo/app/config"
	wooApi "activewear-woo/app/woo"
	"fmt"
	"strconv"
	"sync"
	"time"
)

type ChannelResponse struct {
	Error error
}

func StartSync() error {
	start := time.Now().UTC().Add(3 * time.Hour)
	shouldCreate := make(map[int]activewear.Style)
	shouldUpdate := make(map[int]activewear.Style)

	wooClient, wooCategories, wooProducts, err := GetWooData()
	if err != nil {
		return err
	}

	styles, categories, products, err := GetActiveWearData()
	if err != nil {
		return err
	}

	wooCategories, err = SynchronizeCategories(wooCategories, categories, wooClient)
	if err != nil {
		return err
	}

	variationsMap := GetVariationsMap(styles, products)

	//Divide style for 2 arrays
	for _, s := range styles {
		if !ContainsProduct(wooProducts, strconv.Itoa(s.Styleid)) {
			shouldCreate[s.Styleid] = s
		} else {
			shouldUpdate[s.Styleid] = s
		}
	}

	if len(shouldCreate) > 0 {
		wooProductsToCreate, err := CreateWooProductsFromErply(shouldCreate, wooApi.Create, products, variationsMap, categories, wooCategories, wooProducts, wooClient)
		if err != nil {
			return err
		}

		createdProducts, err := CreateProducts(wooProductsToCreate, wooClient, wooApi.Create)
		if err != nil {
			return err
		}

		var firstBatch []wooApi.Product
		var wg sync.WaitGroup
		for k, c := range createdProducts {
			firstBatch = append(firstBatch, c)
			if (k%5 == 0 && k != 0) || k == len(createdProducts)-1 {
				fmt.Println("Updated " + strconv.Itoa(k))
				err = VariationsSync(firstBatch, &wg, wooClient, styles, variationsMap, wooProducts)
				if err != nil {
					fmt.Println("SET TIMEOUT FOR 3 min")
					time.Sleep(3 * time.Minute)
					fmt.Println("RETRYYY")
					wooClient = wooApi.WooClient{Client: wooApi.CreateClient(config.Configuration.Settings.Url, config.Configuration.Settings.ConsumerKey, config.Configuration.Settings.ConsumerSecret)}
					err := VariationsSync(firstBatch, &wg, wooClient, styles, variationsMap, wooProducts)
					if err != nil {
						wooClient = wooApi.WooClient{Client: wooApi.CreateClient(config.Configuration.Settings.Url, config.Configuration.Settings.ConsumerKey, config.Configuration.Settings.ConsumerSecret)}
						time.Sleep(3 * time.Minute)
						fmt.Println("CONTINUE")
						continue
					}
				}
				firstBatch = nil
			}
		}
	}


	if len(shouldUpdate) > 0 {
		wooProductsToUpdate, err := CreateWooProductsFromErply(shouldUpdate, wooApi.Update, products, variationsMap, categories, wooCategories, wooProducts, wooClient)
		if err != nil {
			return err
		}

		updateProducts, err := CreateProducts(wooProductsToUpdate, wooClient, wooApi.Update)
		if err != nil {
			return err
		}

		var firstBatch []wooApi.Product
		var wg sync.WaitGroup
		for k, c := range updateProducts {
			firstBatch = append(firstBatch, c)
			if (k%5 == 0 && k != 0) || k == len(updateProducts)-1 {
				fmt.Println("Updated " + strconv.Itoa(k))
				err = VariationsSync(firstBatch, &wg, wooClient, styles, variationsMap, wooProducts)
				if err != nil {
					fmt.Println("SET TIMEOUT FOR 3 min")
					time.Sleep(3 * time.Minute)
					fmt.Println("RETRYYY")
					wooClient = wooApi.WooClient{Client: wooApi.CreateClient(config.Configuration.Settings.Url, config.Configuration.Settings.ConsumerKey, config.Configuration.Settings.ConsumerSecret)}
					err := VariationsSync(firstBatch, &wg, wooClient, styles, variationsMap, wooProducts)
					if err != nil {
						wooClient = wooApi.WooClient{Client: wooApi.CreateClient(config.Configuration.Settings.Url, config.Configuration.Settings.ConsumerKey, config.Configuration.Settings.ConsumerSecret)}
						time.Sleep(3 * time.Minute)
						fmt.Println("CONTINUE")
						continue
					}
				}
				firstBatch = nil
			}
		}
	}

	fmt.Println("GMT+2:", start)
	stop := time.Now().UTC().Add(3 * time.Hour)
	fmt.Println("GMT+2:", stop)

	return nil
}



func VariationsSync(firstBatch []wooApi.Product, wg *sync.WaitGroup, wooClient wooApi.WooClient, styles []activewear.Style, variationsMap map[string][]activewear.Product, wooProducts []wooApi.Product) error {
	errorChannel := make(chan *ChannelResponse, len(firstBatch))
	for _, cp := range firstBatch {
		wg.Add(1)
		go func(createdProduct wooApi.Product, client wooApi.WooClient, rStyles []activewear.Style, wProducts []wooApi.Product) {
			defer wg.Done()
			var variations []activewear.Product
			styleID := GetMetaDataValueByKey(createdProduct.MetaData, wooApi.ProductID)
			if val, ok := variationsMap[styleID]; ok {
				variations = val
			} else {
				return
			}

			style := GetStyleFromSliceByID(styleID, rStyles)
			var wooVariations []wooApi.VaritionInfo
			for _, w := range wProducts {
				if w.ID == createdProduct.ID {
					wooVariations = w.VariationsInfo
					break
				}
			}

			wooAllVariationsToProccess, err := CreateWooVariationsFromErply(variations, client, style)
			if err != nil {
				er := &ChannelResponse{Error: err}
				errorChannel <- er
				return
			}

			if len(wooAllVariationsToProccess) == 0 {
				return
			}

			var wooVariationsToCreate []wooApi.Variation
			var wooVariationsToUpdate []wooApi.Variation

			for _, w := range wooAllVariationsToProccess {
				if w.AWID == 0 {
					continue
				}
				variationID, exists := CheckIfVariationExists(wooVariations, strconv.Itoa(w.AWID))
				if exists {
					withID := w
					withID.ID = variationID
					wooVariationsToUpdate = append(wooVariationsToUpdate, withID)
				} else {
					wooVariationsToCreate = append(wooVariationsToCreate, w)
				}
			}

			var batch []wooApi.Variation
			for a, wv := range wooVariationsToCreate {
				batch = append(batch, wv)
				if (a%99 == 0 && a != 0) || a == len(wooVariationsToCreate)-1 {
					_, err = client.BatchVariationOperation(wooApi.Create, batch, nil, createdProduct.ID)
					if err != nil {
						er := &ChannelResponse{Error: err}
						errorChannel <- er
						return
					}
					batch = nil
				}
			}

			batch = nil
			for a, wv := range wooVariationsToUpdate {
				batch = append(batch, wv)
				if (a%99 == 0 && a != 0) || a == len(wooVariationsToUpdate)-1 {
					_, err = client.BatchVariationOperation(wooApi.Update, batch, nil, createdProduct.ID)
					if err != nil {
						er := &ChannelResponse{Error: err}
						errorChannel <- er
						return
					}
					batch = nil
				}
			}
		}(cp, wooClient, styles, wooProducts)
	}
	wg.Wait()
	close(errorChannel)
	chanErr := <-errorChannel
	if chanErr != nil && chanErr.Error != nil {
		return chanErr.Error
	}

	return nil
}

func CreateProducts(wooProductsToCreate []wooApi.Product, client wooApi.WooClient, operation string) ([]wooApi.Product, error) {
	var batch []wooApi.Product
	var resp []wooApi.Product
	for a, wp := range wooProductsToCreate {
		batch = append(batch, wp)
		if (a%99 == 0 && a != 0) || a == len(wooProductsToCreate)-1 {
			fmt.Println("Update PRODUCTs " + strconv.Itoa(a))
			res, err := client.BatchProductOperation(operation, batch, nil)
			if err != nil {
				return nil, err
			}

			if res == nil {
				fmt.Println("IT IS BATCH")
				fmt.Println(len(batch))
				fmt.Println(batch)
				continue
			}

			if operation == wooApi.Create {
				resp = append(resp, res.Create...)
			} else {
				resp = append(resp, res.Update...)
			}

			batch = nil
		}
	}

	return resp, nil
}

func GetVariationsMap(styles []activewear.Style, products []activewear.Product) map[string][]activewear.Product {
	variationsMap := make(map[string][]activewear.Product)
	for _, style := range styles {
		var variations []activewear.Product
		for _, product := range products {
			if style.Styleid == product.Styleid {
				variations = append(variations, product)
			}
		}
		variationsMap[strconv.Itoa(style.Styleid)] = variations
	}

	return variationsMap
}

func GetFullListOfProducts(styles []activewear.Style) ([]activewear.Product, error) {
	var allProducts []activewear.Product
	str := ""
	for a, style := range styles{
		str += strconv.Itoa(style.Styleid) + ","
		if (a%200 == 0 && a != 0) || a == len(styles)-1 {
			fmt.Println(a)
			//fmt.Println(str)
			products, err := activewear.GetProductsByStyleID(str)
			if err != nil {
				return nil, err
			}
			allProducts = append(allProducts, products...)
			str = ""
		}
	}

	return allProducts, nil
}

func GetAllWoocommerceCategories(wooClient wooApi.WooClient) ([]wooApi.Category, error) {
	var allWooCategories []wooApi.Category
	pageNr := 1
	for {
		wooCategories, err := wooClient.GetAllCategories(pageNr)
		pageNr += 1
		if err != nil {
			return nil, err
		}

		if len(wooCategories) == 0 {
			break
		}

		allWooCategories = append(allWooCategories, wooCategories...)
	}

	return allWooCategories, nil
}

func GetAllWoocommerceProducts(wooClient wooApi.WooClient) ([]wooApi.Product, error) {
	var allWooProducts []wooApi.Product
	pageNr := 1
	for {
		wooProducts, err := wooClient.GetAllProducts(pageNr)
		pageNr += 1
		if err != nil {
			return nil, err
		}

		if len(wooProducts) == 0 {
			break
		}

		allWooProducts = append(allWooProducts, wooProducts...)

		if len(wooProducts) < 99 {
			break
		}
	}

	return allWooProducts, nil
}

func SynchronizeCategories(wooCategories []wooApi.Category, categories []activewear.Category, client wooApi.WooClient) ([]wooApi.Category, error) {
	var categoriesToCreate []wooApi.Category
	for _, c := range categories {
		if !WooContainsCategoryWithSlug(wooCategories, strconv.Itoa(c.Categoryid)) {
			newC := wooApi.Category{
				Name: c.Name,
				Slug: strconv.Itoa(c.Categoryid),
			}
			categoriesToCreate = append(categoriesToCreate, newC)
		}
	}

	var batch []wooApi.Category
	for a, cc := range categoriesToCreate {
		batch = append(batch, cc)
		if (a%99 == 0 && a != 0) || a == len(categoriesToCreate)-1 {
			res, err := client.BatchCategoryOperation(wooApi.Create, batch, nil)
			if err != nil {
				return nil, err
			}

			wooCategories = append(wooCategories, res.Create...)
			batch = nil
		}
	}
	return wooCategories, nil
}

func GetWooData() (wooApi.WooClient, []wooApi.Category, []wooApi.Product, error) {
	wooClient := wooApi.WooClient{Client: wooApi.CreateClient(config.Configuration.Settings.Url, config.Configuration.Settings.ConsumerKey, config.Configuration.Settings.ConsumerSecret)}
	wooCategories, err := GetAllWoocommerceCategories(wooClient)
	if err != nil {
		return wooClient, nil, nil, err
	}

	wooProducts, err := GetAllWoocommerceProducts(wooClient)
	if err != nil {
		return wooClient, nil, nil, err
	}

	return wooClient, wooCategories, wooProducts, nil
}

func GetActiveWearData() ([]activewear.Style, []activewear.Category, []activewear.Product, error) {
	styles, err := activewear.GetStyles()
	if err != nil {
		return nil, nil, nil, err
	}

	//var st []activewear.Style
	//for a, u := range styles {
	//	st = append(st, u)
	//	if a == 100 {
	//		break
	//	}
	//}

	categories, err := activewear.GetCategories()
	if err != nil {
		return nil, nil, nil, err
	}

	//specs, err := activewear.GetSpecsByStyleID("39")
	//if err != nil {
	//	return err
	//}

	products, err := GetFullListOfProducts(styles)
	if err != nil {
		return nil, nil, nil, err
	}

	return styles, categories, products, nil
}
