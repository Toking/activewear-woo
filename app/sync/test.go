package sync
//
//import (
//	"activewear-woo/app/activewear"
//	"activewear-woo/app/config"
//	wooApi "activewear-woo/app/woo"
//	"fmt"
//	"strconv"
//	"sync"
//	"time"
//)
//
//func StartSync() error {
//	start := time.Now().UTC().Add(3 * time.Hour)
//	var shouldUpdate []activewear.Style
//	styles, err := activewear.GetStyles()
//	if err != nil {
//		return err
//	}
//
//	products, err := GetFullListOfProducts(styles)
//	if err != nil {
//		return err
//	}
//
//	wooClient := wooApi.WooClient{Client: wooApi.CreateClient(config.Configuration.Settings.Url, config.Configuration.Settings.ConsumerKey, config.Configuration.Settings.ConsumerSecret)}
//	wooPro, err := GetAllWoocommerceProducts(wooClient)
//	if err != nil {
//		return err
//	}
//
//	var wooProducts []wooApi.Product
//	for _, o := range wooPro {
//		if o.VariationsInfo == nil || len(o.VariationsInfo) == 0 {
//			wooProducts = append(wooProducts, o)
//		}
//	}
//
//	variationsMap := GetVariationsMap(styles, products)
//
//	for _, s := range styles {
//		id := GetWooProductIDByAWID(wooProducts, strconv.Itoa(s.Styleid))
//		//p := GetWooProductByAWID(wooProducts, strconv.Itoa(s.Styleid))
//		if id != 0 {
//			updatedStyle := s
//			updatedStyle.WooID = id
//			var variations []activewear.Product
//			if val, ok := variationsMap[strconv.Itoa(s.Styleid)]; ok {
//				variations = val
//			}
//			if len(variations) > 0 {
//				shouldUpdate = append(shouldUpdate, updatedStyle)
//			}
//
//			//if len(variations) != len(p.VariationsInfo) {
//			//	shouldUpdate = append(shouldUpdate, updatedStyle)
//			//}
//		}
//	}
//
//	fmt.Println("Lennnn")
//	fmt.Println(len(shouldUpdate))
//
//	if len(shouldUpdate) > 0 {
//		var firstBatch []activewear.Style
//		var wg sync.WaitGroup
//		for k, c := range shouldUpdate {
//			firstBatch = append(firstBatch, c)
//			if (k%5 == 0 && k != 0) || k == len(shouldUpdate)-1 {
//				fmt.Println("Updated " + strconv.Itoa(k))
//				err = VariationsSync(firstBatch, &wg, wooClient, variationsMap, wooProducts)
//				if err != nil {
//					fmt.Println("SET TIMEOUT FOR 30 SEC")
//					time.Sleep(3 * time.Minute)
//					fmt.Println("RETRYYY")
//					wooClient = wooApi.WooClient{Client: wooApi.CreateClient(config.Configuration.Settings.Url, config.Configuration.Settings.ConsumerKey, config.Configuration.Settings.ConsumerSecret)}
//					err := VariationsSync(firstBatch, &wg, wooClient, variationsMap, wooProducts)
//					if err != nil {
//						wooClient = wooApi.WooClient{Client: wooApi.CreateClient(config.Configuration.Settings.Url, config.Configuration.Settings.ConsumerKey, config.Configuration.Settings.ConsumerSecret)}
//						time.Sleep(3 * time.Minute)
//						fmt.Println("CONTINUE")
//						continue
//					}
//				}
//				firstBatch = nil
//			}
//		}
//	}
//
//	fmt.Println("GMT+2:", start)
//	stop := time.Now().UTC().Add(3 * time.Hour)
//	fmt.Println("GMT+2:", stop)
//
//	return nil
//}
//
//func VariationsSync(firstBatch []activewear.Style, wg *sync.WaitGroup, wooClient wooApi.WooClient, variationsMap map[string][]activewear.Product, wooProducts []wooApi.Product) error {
//	errorChannel := make(chan *ChannelResponse, len(firstBatch))
//	for _, cp := range firstBatch {
//		wg.Add(1)
//		go func(style activewear.Style, client wooApi.WooClient, wProducts []wooApi.Product) {
//			defer wg.Done()
//			var variations []activewear.Product
//			if val, ok := variationsMap[strconv.Itoa(style.Styleid)]; ok {
//				variations = val
//			} else {
//				return
//			}
//
//			var wooVariations []wooApi.VaritionInfo
//			for _, w := range wProducts {
//				if w.ID == style.WooID {
//					wooVariations = w.VariationsInfo
//					break
//				}
//			}
//
//			wooAllVariationsToProccess, err := CreateWooVariationsFromErply(variations, client, &style)
//			if err != nil {
//				er := &ChannelResponse{Error: err}
//				errorChannel <- er
//				return
//			}
//
//			if len(wooAllVariationsToProccess) == 0 {
//				return
//			}
//
//			var wooVariationsToCreate []wooApi.Variation
//
//			for _, w := range wooAllVariationsToProccess {
//				if w.AWID == 0 {
//					continue
//				}
//				_, exists := CheckIfVariationExists(wooVariations, strconv.Itoa(w.AWID))
//				if !exists {
//					wooVariationsToCreate = append(wooVariationsToCreate, w)
//				}
//			}
//
//			var batch []wooApi.Variation
//			for a, wv := range wooVariationsToCreate {
//				batch = append(batch, wv)
//				if (a%99 == 0 && a != 0) || a == len(wooVariationsToCreate)-1 {
//					_, err = client.BatchVariationOperation(wooApi.Create, batch, nil, style.WooID)
//					if err != nil {
//						er := &ChannelResponse{Error: err}
//						errorChannel <- er
//						return
//					}
//					batch = nil
//				}
//			}
//		}(cp, wooClient, wooProducts)
//	}
//	wg.Wait()
//	close(errorChannel)
//	chanErr := <-errorChannel
//	if chanErr != nil && chanErr.Error != nil {
//		return chanErr.Error
//		//if err != nil {
//		//	return nil
//		//}
//	}
//
//	return nil
//}