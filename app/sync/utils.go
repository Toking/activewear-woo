package sync

import (
	"activewear-woo/app/activewear"
	wooApi "activewear-woo/app/woo"
	"fmt"
	"strconv"
	"strings"
)

func GetWoocommerceImageIDByErplyImageName(wooImages []wooApi.Image, imageName string) int {
	for _, i := range wooImages {
		if strings.Contains(i.Name, imageName) {
			return i.Id
		}
	}

	return 0
}

func WooGetCategoryWithSlug(categories []wooApi.Category, slug string) *wooApi.Category {
	for _, i := range categories {
		if i.Slug == slug {
			return &i
		}
	}
	return nil
}

func WooContainsCategoryWithSlug(categories []wooApi.Category, slug string) bool{
	for _, i := range categories {
		if i.Slug == slug {
			return true
		}
	}
	return false
}

func ContainsProduct(s []wooApi.Product, e string) bool {
	for _, a := range s {
		productID := GetMetaDataValueByKey(a.MetaData, wooApi.ProductID)
		if productID == e {
			return true
		}
	}
	return false
}

func GetMetaDataValueByKey(metadata []wooApi.MetaData, key string) string {
	for _, a := range metadata {
		if strings.ToLower(a.Key) == strings.ToLower(key) {
			return fmt.Sprintf("%v", a.Value)
		}
	}
	return "0"
}

func GetWooProductIDByAWID(products []wooApi.Product, e string) int {
	for _, i := range products {
		if len(i.MetaData) != 0 {
			if e == GetMetaDataValueByKey(i.MetaData, wooApi.ProductID) {
				return i.ID
			}
		}
	}
	return 0
}

func GetWooProductByAWID(products []wooApi.Product, e string) *wooApi.Product {
	for _, i := range products {
		if len(i.MetaData) != 0 {
			a := i
			if e == GetMetaDataValueByKey(i.MetaData, wooApi.ProductID) {
				return &a
			}
		}
	}
	return nil
}

func GetAttributeByName(attributes []wooApi.Attribute, name string) *wooApi.Attribute {
	for _, i := range attributes {
		if i.Name == name {
			return &i
		}
	}

	return nil
}

func ContainsAttribute(attributes []wooApi.Attribute, name string) bool {
	for _, i := range attributes {
		if i.Name == name {
			return true
		}
	}

	return false
}

func CheckAndUpdateAttributeOptions(attributes []wooApi.Attribute, option string, name string) []wooApi.Attribute {
	for key, i := range attributes {
		if i.Name == name && !SliceContainsString(i.Options, option) {
			updatedAttributes := make([]string, len(i.Options))
			copy(updatedAttributes, i.Options)
			updatedAttributes = append(updatedAttributes, option)
			attributes[key].Options = updatedAttributes
		}
	}
	return attributes
}

func SliceContainsString(s []string, e string) bool {
	for _, a := range s {
		if strings.ToLower(a) == strings.ToLower(e) {
			return true
		}
	}
	return false
}

func GetStyleFromSliceByID(styleID string, styles []activewear.Style) *activewear.Style {
	for _, s := range styles {
		if strconv.Itoa(s.Styleid) == styleID {
			 return &s
		}
	}

	return nil
}

func CheckIfVariationExists(variations []wooApi.VaritionInfo, variationID string) (int, bool) {
	for _, i := range variations {
		if GetMetaDataValueByKey(i.MetaData, wooApi.ProductID) == variationID {
			return i.ID, true
		}
	}

	return 0, false
}