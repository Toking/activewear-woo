package activewear

import (
	"activewear-woo/app/config"
	"activewear-woo/app/request"
	"activewear-woo/app/request/httpClient"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
)

func GetStyles() ([]Style, error) {
	client := httpClient.New(0)
	req, err := request.GetHTTPRequest(ActiveWearAPI + StyleURI)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(config.Configuration.Settings.AwUsername, config.Configuration.Settings.AwKey)
	req.Header.Add("Accept", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var res []Style
	err = json.Unmarshal(bodyBytes, &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func GetProductsByStyleID(styleID string) ([]Product, error)  {
	client := httpClient.New(0)
	req, err := request.GetHTTPRequest(ActiveWearAPI + ProductURI)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(config.Configuration.Settings.AwUsername, config.Configuration.Settings.AwKey)
	req.Header.Add("Accept", "application/json")

	params := &url.Values{}
	params.Add("style", styleID)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var res []Product
	err = json.Unmarshal(bodyBytes, &res)
	if err != nil {
		fmt.Println(string(bodyBytes))
		return nil, err
	}

	return res, nil
}

func GetSpecsByStyleID(styleID string) ([]Specs, error) {
	client := httpClient.New(0)
	req, err := request.GetHTTPRequest(ActiveWearAPI + SpecsURI)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(config.Configuration.Settings.AwUsername, config.Configuration.Settings.AwKey)
	req.Header.Add("Accept", "application/json")

	params := &url.Values{}
	params.Add("style", styleID)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var res []Specs
	err = json.Unmarshal(bodyBytes, &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func GetCategories() ([]Category, error) {
	client := httpClient.New(0)
	req, err := request.GetHTTPRequest(ActiveWearAPI + CategoriesURI)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(config.Configuration.Settings.AwUsername, config.Configuration.Settings.AwKey)
	req.Header.Add("Accept", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var res []Category
	err = json.Unmarshal(bodyBytes, &res)
	if err != nil {
		return nil, err
	}

	return res, nil
}