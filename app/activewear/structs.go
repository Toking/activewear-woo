package activewear

type Style struct {
	WooID           int    `json:"-"`
	Styleid           int    `json:"styleID"`
	Partnumber        string `json:"partNumber"`
	Brandname         string `json:"brandName"`
	Stylename         string `json:"styleName"`
	Uniquestylename   string `json:"uniqueStyleName"`
	Title             string `json:"title"`
	Description       string `json:"description"`
	Basecategory      string `json:"baseCategory"`
	Categories        string `json:"categories"`
	Catalogpagenumber string `json:"catalogPageNumber"`
	Newstyle          bool   `json:"newStyle"`
	Comparablegroup   int    `json:"comparableGroup"`
	Companiongroup    int    `json:"companionGroup"`
	Brandimage        string `json:"brandImage"`
	Styleimage        string `json:"styleImage"`
	Prop65Chemicals   string `json:"prop65Chemicals"`
	Noeretailing      bool   `json:"noeRetailing"`
	Boxrequired       bool   `json:"boxRequired"`
}

type Product struct {
	Sku                  string  `json:"sku"`
	Gtin                 string  `json:"gtin"`
	SkuidMaster          int     `json:"skuID_Master"`
	Yoursku              string  `json:"yourSku"`
	Styleid              int     `json:"styleID"`
	Brandname            string  `json:"brandName"`
	Stylename            string  `json:"styleName"`
	Colorname            string  `json:"colorName"`
	Colorcode            string  `json:"colorCode"`
	Colorpricecodename   string  `json:"colorPriceCodeName"`
	Colorgroup           string  `json:"colorGroup"`
	Colorgroupname       string  `json:"colorGroupName"`
	Colorfamilyid        string  `json:"colorFamilyID"`
	Colorfamily          string  `json:"colorFamily"`
	Colorswatchimage     string  `json:"colorSwatchImage"`
	Colorswatchtextcolor string  `json:"colorSwatchTextColor"`
	Colorfrontimage      string  `json:"colorFrontImage"`
	Colorsideimage       string  `json:"colorSideImage"`
	Colorbackimage       string  `json:"colorBackImage"`
	Colordirectsideimage string  `json:"colorDirectSideImage"`
	Color1               string  `json:"color1"`
	Color2               string  `json:"color2"`
	Sizename             string  `json:"sizeName"`
	Sizecode             string  `json:"sizeCode"`
	Sizeorder            string  `json:"sizeOrder"`
	Sizepricecodename    string  `json:"sizePriceCodeName"`
	Caseqty              int     `json:"caseQty"`
	Unitweight           float64 `json:"unitWeight"`
	Mapprice             float64 `json:"mapPrice"`
	Pieceprice           float64 `json:"piecePrice"`
	Dozenprice           float64 `json:"dozenPrice"`
	Caseprice            float64 `json:"casePrice"`
	Saleprice            float64 `json:"salePrice"`
	Customerprice        float64 `json:"customerPrice"`
	Noeretailing         bool    `json:"noeRetailing"`
	Caseweight           float64 `json:"caseWeight"`
	Casewidth            float64 `json:"caseWidth"`
	Caselength           float64 `json:"caseLength"`
	Caseheight           float64 `json:"caseHeight"`
	Qty                  int     `json:"qty"`
	Countryoforigin      string  `json:"countryOfOrigin"`
	Warehouses           []Warehouse `json:"warehouses"`
}

type Warehouse struct {
	Warehouseabbr      string `json:"warehouseAbbr"`
	Skuid              int    `json:"skuID"`
	Qty                int    `json:"qty"`
	Closeout           bool   `json:"closeout"`
	Dropship           bool   `json:"dropship"`
	Excludefreefreight bool   `json:"excludeFreeFreight"`
	Fullcaseonly       bool   `json:"fullCaseOnly"`
	Returnable         bool   `json:"returnable"`
	Expectedinventory  string `json:"expectedInventory"`
}

type Category struct {
	Categoryid int    `json:"categoryID"`
	Name       string `json:"name"`
	Image      string `json:"image"`
}

type Specs struct {
	Specid     int    `json:"specID"`
	Styleid    int    `json:"styleID"`
	Partnumber string `json:"partNumber"`
	Brandname  string `json:"brandName"`
	Stylename  string `json:"styleName"`
	Sizename   string `json:"sizeName"`
	Sizeorder  string `json:"sizeOrder"`
	Specname   string `json:"specName"`
	Value      string `json:"value"`
}