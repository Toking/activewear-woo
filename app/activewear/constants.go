package activewear

const (
	ActiveWearAPI = "https://api.ssactivewear.com/v2/"
	CDN           = "https://cdn.ssactivewear.com/"
	StyleURI = "styles"
	CategoriesURI = "categories"
	ProductURI = "products"
	SpecsURI = "specs"

)

