package httpClient

import (
	"net"
	"net/http"
	"time"
)

const (
	timeout               = 20 * time.Minute
	keepAlive             = 20 * time.Minute
	tlsHandshakeTimeout   = 20 * time.Minute
	expectContinueTimeout = 20 * time.Minute
	responseHeaderTimeout = 20 * time.Minute
	httpClientTimeout     = 20 * time.Minute
	maxIdleConns          = 2000
	maxConnsPerHost       = 2000
)

//Client ...
type Client struct {
	//HttpClient ...
	Cli *http.Client
}

//New HTTPClient ...
func New(customTimeout int) *Client {
	tr := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   timeout,
			KeepAlive: keepAlive,
		}).DialContext,
		TLSHandshakeTimeout: tlsHandshakeTimeout,

		ExpectContinueTimeout: expectContinueTimeout,
		ResponseHeaderTimeout: responseHeaderTimeout,

		MaxIdleConns:    maxIdleConns,
		MaxConnsPerHost: maxConnsPerHost,
	}
	cli := Client{
		Cli: &http.Client{
			Transport: tr,
			Timeout:   httpClientTimeout,
		},
	}

	if customTimeout != 0 {
		cli.Cli.Timeout = time.Duration(customTimeout) * time.Second
	}

	return &cli
}

//Do request
func (cli *Client) Do(req *http.Request) (*http.Response, error) {
	//start := time.Now()
	resp, err := cli.Cli.Do(req)
	//elapsed := time.Since(start)
	//log.HTTP("httpClient", req, resp, err, elapsed, req.Method)
	return resp, err
}
