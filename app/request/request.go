package request

import (
	"io"
	"net/http"
)

func GetHTTPRequest(url string) (*http.Request, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err

	}
	return req, err
}

func PostHTTPRequest(url string, reader io.Reader) (*http.Request, error) {
	req, err := http.NewRequest("POST", url, reader)
	if err != nil {
		return nil, err

	}
	return req, err
}

func PutHTTPRequest(url string, reader io.Reader) (*http.Request, error) {
	req, err := http.NewRequest("PUT", url, reader)
	if err != nil {
		return nil, err

	}
	return req, err
}
