package woo

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
)

func (c *WooClient) GetAllAttributes() ([]Attribute, error) {
	if r, err := c.Client.Get(Products+Attributes, nil); err != nil {
		if err == io.EOF {
			err = nil
		}
		return nil, err
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err == io.EOF {
			err = nil
		}
		if string(bodyBytes) == "[]" || string(bodyBytes) == "{}" || string(bodyBytes) == "" {
			return nil, nil
		}
		if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
			return nil, nil
			//return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}

		if err != nil {
			return nil, err
		}

		var res []Attribute
		err = json.Unmarshal(bodyBytes, &res)
		if err != nil {
			return nil, err
		}

		return res, nil

	}
}

func (c *WooClient) AddAttribute(attribute *Attribute) (*Attribute, error) {
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(attribute)
	if r, err := c.Client.Post(Products+Attributes, nil, b); err != nil {
		if err == io.EOF {
			err = nil
		}
		return nil, err
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err == io.EOF {
			err = nil
		}

		if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}

		if string(bodyBytes) == "[]" || string(bodyBytes) == "{}" || string(bodyBytes) == "" {
			return nil, nil
		}
		if err != nil {
			return nil, err
		}

		var res Attribute
		err = json.Unmarshal(bodyBytes, &res)
		if err != nil {
			return nil, err
		}

		return &res, nil
	}
}

func (c *WooClient) GetAllPaymentGateways() ([]*PaymentGateway, error) {
	if r, err := c.Client.Get(PaymentGateways, nil); err != nil {
		if err == io.EOF {
			err = nil
		}
		return nil, err
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err == io.EOF {
			err = nil
		}
		if string(bodyBytes) == "[]" || string(bodyBytes) == "{}" || string(bodyBytes) == "" {
			return nil, nil
		}
		if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}

		if err != nil {
			return nil, err
		}

		var res []*PaymentGateway
		err = json.Unmarshal(bodyBytes, &res)
		if err != nil {
			return nil, err
		}

		return res, nil

	}
}
