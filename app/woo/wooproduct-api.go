package woo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"activewear-woo/pkg/woocom-lib/client"
	"activewear-woo/pkg/woocom-lib/options"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

type WooClient struct {
	Client *client.Client
}

func CreateClient(url string, key string, secret string) *client.Client {
	factory := client.Factory{} // client.Factory{}
	if url == "runstore.co.za" {
		url = "www.runstore.co.za"
	}
	url = "https://" + url
	c := factory.NewClient(options.Basic{ // factory.NewClient(options.Basic{
		URL:    url,
		Key:    key,
		Secret: secret,
		Options: options.Advanced{
			WPAPI:           true,
			WPAPIPrefix:     "/wp-json/",
			Version:         "wc/v3",
			QueryStringAuth: true,
			Timeout:         100,
		},
	})

	return &c
}

func (c *WooClient) DeleteProduct(productID string) {
	if r, err := c.Client.Delete(Products+productID, nil, nil); err != nil {
		return
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		return
	} else {
		defer r.Body.Close()
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return
		} else {
			fmt.Println(string(bodyBytes))
		}
	}
}

func (c *WooClient) UpdateProduct(product interface{}) error {
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(product)
	if r, err := c.Client.Put(Products+"34", nil, b); err != nil {
		return err
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		return err
	} else {
		defer r.Body.Close()
		if _, err := ioutil.ReadAll(r.Body); err != nil {
			return err
		} else {
			return nil
		}
	}
}

func (c *WooClient) AddProduct(product Product) {
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(product)
	if r, err := c.Client.Post(Products, nil, b); err != nil {
		return
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return
		} else {
			fmt.Println(bodyBytes)
		}
		return
	} else {
		defer r.Body.Close()
		if _, err := ioutil.ReadAll(r.Body); err != nil {
			return
		}
	}
}

func (c *WooClient) GetProductByID(productID string) (*Product, error) {
	if r, err := c.Client.Get(Products+productID, nil); err != nil {
		return nil, err
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return nil, err
		} else {
			fmt.Println(bodyBytes)
		}
		return nil, err
	} else {
		defer r.Body.Close()
		var res *Product
		if err := json.NewDecoder(r.Body).Decode(&res); err != nil {
			return nil, err
		}
		return res, nil
	}
}

func (c *WooClient) GetAllProducts(pageNr int) ([]Product, error) {
	values := url.Values{}
	values.Add("per_page", "100")
	values.Add("page", strconv.Itoa(pageNr))

	if r, err := c.Client.Get(Products, values); err != nil {
		return nil, err
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err == io.EOF {
			err = nil
		}
		if string(bodyBytes) == "[]" || string(bodyBytes) == "{}" || string(bodyBytes) == "" {
			return nil, nil
		}
		if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}

		if err != nil {
			return nil, err
		}

		var res []Product
		err = json.Unmarshal(bodyBytes, &res)
		if err != nil {
			return nil, err
		}

		return res, nil

	}
}

func (c *WooClient) BatchProductOperation(operation string, products []Product, toDelete []int) (*WooProductBatchObjResponse, error) {
	var batch WooProductBatchObj
	switch operation {
	case Create:
		batch.Create = products
	case Update:
		batch.Update = products
	case Delete:
		batch.Delete = toDelete
	}

	b := &bytes.Buffer{}
	err := json.NewEncoder(b).Encode(batch)
	if err != nil {
		return nil, NewWooError("500", err.Error())
	}

	if r, err := c.Client.Post(Products+Batch, nil, b); err != nil {
		return nil, err
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err == io.EOF {
			err = nil
		}
		if err != nil {
			return nil, err
		}

		if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
			fmt.Println(r.StatusCode)
			fmt.Println( " HERE")
			return nil, nil
			//return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
		if string(bodyBytes) == "[]" || string(bodyBytes) == "{}" || string(bodyBytes) == "" {
			//fmt.Println(string(bodyBytes))
			fmt.Println( " HERE1")
			return nil, nil
		}

		res := &WooProductBatchObjResponse{}
		if err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&res); err != nil {
			fmt.Println( " HERE2")
			return nil, err
		}

		return res, nil
	}
}
