package woo

import (
"encoding/json"
)

type WooTag struct {
	Description string `json:"description,omitempty"`
	Slug        string `json:"slug,omitempty"`
	Name        string `json:"name,omitempty"`
	ID          int    `json:"id,omitempty"`
}

type Product struct {
	ID int `json:"id,omitempty"`
	//draft, pending, private and publish
	Status       string      `json:"status,omitempty"`
	Name         string      `json:"name,omitempty"`
	Categories   []Category  `json:"categories,omitempty"`
	RegularPrice interface{} `json:"regular_price,omitempty"`
	SalePrice    interface{} `json:"sale_price"`
	Images       []Image     `json:"images,omitempty"`
	Weight       string      `json:"weight,omitempty"`
	//instock, outofstock, onbackorder
	StockStatus      string      `json:"stock_status,omitempty"`
	ManageStock      bool        `json:"manage_stock"`
	StockQuantity    interface{} `json:"stock_quantity,omitempty"`
	Sku              string      `json:"sku,omitempty"`
	ShortDescription *string     `json:"short_description,omitempty"`
	Description      string     `json:"description,omitempty"`
	Slug             string      `json:"slug,omitempty"`
	Permalink        string      `json:"permalink,omitempty"`
	//types: simple, grouped, external and variable
	Type     string `json:"type,omitempty"`
	Featured bool   `json:"featured,omitempty"`
	//visible, catalog, search and hidden
	CatalogVisibility string      `json:"catalog_visibility,omitempty"`
	Price             interface{} `json:"price,omitempty"`
	DateOnSaleFromGMT interface{} `json:"date_on_sale_from_gmt"`
	DateOnSaleToGMT   interface{} `json:"date_on_sale_to_gmt"`
	//taxable, shipping and none
	TaxStatus  string         `json:"tax_status,omitempty"`
	Tags       []WooTag       `json:"tags,omitempty"`
	TaxClass   string         `json:"tax_class,omitempty"`
	MetaData   []MetaData     `json:"meta_data,omitempty"`
	Dimensions *WooDimensions `json:"dimensions,omitempty"`
	ParentID   int            `json:"parent_id,omitempty"`
	Attributes []Attribute    `json:"attributes,omitempty"`
	Backorders string         `json:"backorders,omitempty"`
	VariationsInfo  []VaritionInfo `json:"product_variations,omitempty"`
}

type VaritionInfo struct {
	ID          int `json:"id"`
	MetaData   []MetaData     `json:"meta_data,omitempty"`
}

type ShippingMethod struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

type TaxClass struct {
	Slug string `json:"slug"`
	Name string `json:"name"`
}

type Tax struct {
	ID       int    `json:"id"`
	Country  string `json:"country"`
	State    string `json:"state"`
	Postcode string `json:"postcode"`
	City     string `json:"city"`
	Rate     string `json:"rate"`
	Name     string `json:"name"`
	Priority int    `json:"priority"`
	Compound bool   `json:"compound"`
	Shipping bool   `json:"shipping"`
	Order    int    `json:"order"`
	Class    string `json:"class"`
	Links    struct {
		Self []struct {
			Href string `json:"href"`
		} `json:"self"`
		Collection []struct {
			Href string `json:"href"`
		} `json:"collection"`
	} `json:"_links"`
}

type VariationForStock struct {
	AWID int `json:"-"`
	ID   int `json:"id,omitempty"`
	StockQuantity interface{} `json:"stock_quantity,omitempty"`
}
type Variation struct {
	AWID int `json:"-"`
	ID int `json:"id,omitempty"`
	//draft, pending, private and publish
	Status       string      `json:"status,omitempty"`
	Name         string      `json:"name,omitempty"`
	Categories   []Category  `json:"categories,omitempty"`
	RegularPrice interface{} `json:"regular_price,omitempty"`
	SalePrice    interface{} `json:"sale_price,omitempty"`
	Image        *Image      `json:"image,omitempty"`
	Weight       string      `json:"weight,omitempty"`
	//instock, outofstock, onbackorder
	StockStatus      string      `json:"stock_status,omitempty"`
	ManageStock      bool        `json:"manage_stock,omitempty"`
	StockQuantity    interface{} `json:"stock_quantity,omitempty"`
	Sku              string      `json:"sku,omitempty"`
	ShortDescription string      `json:"short_description,omitempty"`
	Description      *string     `json:"description,omitempty"`
	Slug             string      `json:"slug,omitempty"`
	Permalink        string      `json:"permalink,omitempty"`
	//types: simple, grouped, external and variable
	Type     string `json:"type,omitempty"`
	Featured bool   `json:"featured,omitempty"`
	//visible, catalog, search and hidden
	CatalogVisibility string      `json:"catalog_visibility,omitempty"`
	Price             interface{} `json:"price,omitempty"`
	DateOnSaleFromGMT interface{} `json:"date_on_sale_from_gmt"`
	DateOnSaleToGMT   interface{} `json:"date_on_sale_to_gmt"`
	//taxable, shipping and none
	TaxStatus  string              `json:"tax_status,omitempty"`
	TaxClass   string              `json:"tax_class,omitempty"`
	MetaData   []MetaData          `json:"meta_data,omitempty"`
	Dimensions *WooDimensions      `json:"dimensions,omitempty"`
	ParentID   int                 `json:"parent_id,omitempty"`
	Attributes []VariationAttibute `json:"attributes,omitempty"`
	Backorders string              `json:"backorders,omitempty"`
}

type GetVariation struct {
	ID int `json:"id,omitempty"`
	//draft, pending, private and publish
	Status       string      `json:"status,omitempty"`
	Name         string      `json:"name,omitempty"`
	Categories   []Category  `json:"categories,omitempty"`
	RegularPrice interface{} `json:"regular_price,omitempty"`
	SalePrice    interface{} `json:"sale_price,omitempty"`
	Image        *Image      `json:"image,omitempty"`
	Weight       string      `json:"weight,omitempty"`
	//instock, outofstock, onbackorder
	StockStatus      string      `json:"stock_status,omitempty"`
	StockQuantity    interface{} `json:"stock_quantity,omitempty"`
	Sku              string      `json:"sku,omitempty"`
	ShortDescription string      `json:"short_description,omitempty"`
	Description      *string     `json:"description,omitempty"`
	Slug             string      `json:"slug,omitempty"`
	Permalink        string      `json:"permalink,omitempty"`
	//types: simple, grouped, external and variable
	Type     string `json:"type,omitempty"`
	Featured bool   `json:"featured,omitempty"`
	//visible, catalog, search and hidden
	CatalogVisibility string      `json:"catalog_visibility,omitempty"`
	Price             interface{} `json:"price,omitempty"`
	DateOnSaleFromGMT interface{} `json:"date_on_sale_from_gmt"`
	DateOnSaleToGMT   interface{} `json:"date_on_sale_to_gmt"`
	//taxable, shipping and none
	TaxStatus  string              `json:"tax_status,omitempty"`
	TaxClass   string              `json:"tax_class,omitempty"`
	MetaData   []MetaData          `json:"meta_data,omitempty"`
	Dimensions *WooDimensions      `json:"dimensions,omitempty"`
	ParentID   int                 `json:"parent_id,omitempty"`
	Attributes []VariationAttibute `json:"attributes,omitempty"`
	Backorders string              `json:"backorders,omitempty"`
}

type Attribute struct {
	ID        int      `json:"id,omitempty"`
	Slug      string   `json:"slug,omitempty"`
	Name      string   `json:"name,omitempty"`
	Position  int      `json:"position,omitempty"`
	Visible   bool     `json:"visible,omitempty"`
	Variation bool     `json:"variation,omitempty"`
	Options   []string `json:"options,omitempty"`
}

type PaymentGateway struct {
	ID          string `json:"id,omitempty"`
	Title       string `json:"title,omitempty"`
	MethodTitle string `json:"method_title,omitempty"`
}

type VariationAttibute struct {
	ID     int    `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Option string `json:"option,omitempty"`
}

type WooDimensions struct {
	Length string `json:"length,omitempty"`
	Width  string `json:"width,omitempty"`
	Height string `json:"height,omitempty"`
}

type MetaData struct {
	ID    int         `json:"id,omitempty"`
	Key   string      `json:"key"`
	Value interface{} `json:"value"`
}

type Image struct {
	Id   int    `json:"id,omitempty"`
	Src  string `json:"src,omitempty"`
	Name string `json:"name,omitempty"`
	Alt  string `json:"alt,omitempty"`
}

type Category struct {
	ErplyID       string   `json:"-"`
	Id            int      `json:"id,omitempty"`
	Name          string   `json:"name,omitempty"`
	Slug          string   `json:"slug,omitempty"`
	Parent        int      `json:"parent,omitempty"`
	Description   string   `json:"description,omitempty"`
	Display       string   `json:"display,omitempty"`
	MenuOrder     int      `json:"menu_order,omitempty"`
	Count         int      `json:"count,omitempty"`
	Images        []Image  `json:"images,omitempty"`
	Error         WooError `error:"images,omitempty"`
	DifferentName bool     `json:"-"`
	//Links         Links      `json:"_links,omitempty"`
}


type WoocommOrder struct {
	Id              int            `json:"id"`
	OrderKey        string         `json:"order_key,omitempty"`
	Status          string         `json:"status,omitempty"`
	Currency        string         `json:"currency,omitempty"`
	ShippingTax     string         `json:"shipping_tax,omitempty"`
	ShippingTotal   string         `json:"shipping_total,omitempty"`
	Total           string         `json:"total,omitempty"`
	DiscountTotal   string         `json:"discount_total,omitempty"`
	PriceIncludeTax bool           `json:"prices_include_tax,omitempty"`
	Billing         Billing        `json:"billing,omitempty"`
	Shipping        Billing        `json:"shipping,omitempty"`
	ShippingLines   []ShippingLine `json:"shipping_lines,omitempty"`
	PaymentMethod   string         `json:"payment_method_title,omitempty"`
	PaymentMethodID string         `json:"payment_method,omitempty"`
	Links           *Links         `json:"_links,omitempty"`
	CustomerId      int            `json:"customer_id,omitempty"`
	TransactionId   string         `json:"transaction_id,omitempty"`
	LineItems       []LineItem     `json:"line_items,omitempty"`
}

type ShippingLine struct {
	Id          int    `json:"id"`
	MethodTitle string `json:"method_title"`
	MethodID    string `json:"method_id"`
	Total       string `json:"total"`
	TotalTax    string `json:"total_tax"`
	Taxes       []struct {
		ID       int    `json:"id"`
		Total    string `json:"total"`
		Subtotal string `json:"subtotal"`
	} `json:"taxes"`
}

type LineItem struct {
	Id          int         `json:"id"`
	Name        string      `json:"name"`
	ProductId   int         `json:"product_id"`
	VariationID int         `json:"variation_id"`
	Quantity    json.Number `json:"quantity"`
	Price       float64     `json:"price"`
	SubTotalTax string      `json:"subtotal_tax"`
	TotalTax    string      `json:"total_tax"`
	Total       string      `json:"total"`
	SubTotal    string      `json:"subtotal"`
	MetaData    []MetaData  `json:"meta_data,omitempty"`
	Taxes       []struct {
		ID       int    `json:"id"`
		Total    string `json:"total"`
		Subtotal string `json:"subtotal"`
	} `json:"taxes"`
}

type Links struct {
	Self []Self `json:"self,omitempty"`
}

type Self struct {
	Href string `json:"href"`
}

type Billing struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Company   string `json:"company"`
	Address1  string `json:"address_1"`
	Address2  string `json:"address_2"`
	PostCode  string `json:"postcode"`
	Country   string `json:"country"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	City      string `json:"city"`
	State     string `json:"state"`
}

type WooProductBatchObj struct {
	Create []Product `json:"create,omitempty"`
	Update []Product `json:"update,omitempty"`
	Delete []int     `json:"delete,omitempty"`
}

type WooTagBatchObj struct {
	Create []WooTag `json:"create,omitempty"`
	Update []WooTag `json:"update,omitempty"`
	Delete []int    `json:"delete,omitempty"`
}

type WooVariationBatchObj struct {
	Create []Variation `json:"create,omitempty"`
	Update []Variation `json:"update,omitempty"`
	Delete []int        `json:"delete,omitempty"`
}

type WooVariationForStockBatchObj struct {
	Create []VariationForStock `json:"create,omitempty"`
	Update []VariationForStock `json:"update,omitempty"`
	Delete []int        `json:"delete,omitempty"`
}

type WooProductBatchObjResponse struct {
	Create []Product `json:"create,omitempty"`
	Update []Product `json:"update,omitempty"`
	Delete []Product `json:"delete,omitempty"`
}

type WooTagBatchObjResponse struct {
	Create []WooTag `json:"create,omitempty"`
	Update []WooTag `json:"update,omitempty"`
	Delete []WooTag `json:"delete,omitempty"`
}

type WooCategoryBatchObjResponse struct {
	Create []Category `json:"create,omitempty"`
	Update []Category `json:"update,omitempty"`
	Delete []Category `json:"delete,omitempty"`
}

type WooCategoryBatchObj struct {
	Create []Category `json:"create,omitempty"`
	Update []Category `json:"update,omitempty"`
	Delete []int      `json:"delete,omitempty"`
}

type WooOrderBatchObj struct {
	Create []WoocommOrder `json:"create,omitempty"`
	Update []WoocommOrder `json:"update,omitempty"`
	Delete []int          `json:"delete,omitempty"`
}

type AdditionalGroupsResponse struct {
	Results []AdditionalGroup `json:"results,omitempty"`
}

type AdditionalGroup struct {
	GroupID   int `json:"group_id,omitempty"`
	ID        int `json:"id,omitempty"`
	ProductID int `json:"product_id,omitempty"`
}

