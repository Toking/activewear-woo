package woo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/copier"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

func (c *WooClient) GetVariationByID(productID string, variationID string) (*Variation, error) {
	if r, err := c.Client.Get(Products+productID+Variations+"/"+variationID, nil); err != nil {
		return nil, err
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return nil, err
		} else {
			fmt.Println(string(bodyBytes))
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
	} else {
		defer r.Body.Close()
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return nil, err
		} else {
			var res *Variation
			if err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&res); err != nil {
				return nil, err
			}
			return res, nil
		}
	}
}

func (c *WooClient) AddVariation(variation *Variation, productID int) {
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(variation)
	if r, err := c.Client.Post(Products+strconv.Itoa(productID)+Variations, nil, b); err != nil {
		return
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return
		} else {
			fmt.Println(bodyBytes)
		}
		return
	} else {
		defer r.Body.Close()
		if _, err := ioutil.ReadAll(r.Body); err != nil {
			return
		}
	}
}

func (c *WooClient) UpdateVariation(variation *Variation, productID int, variationID string) {
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(variation)
	if r, err := c.Client.Put(Products+strconv.Itoa(productID)+Variations+"/"+variationID, nil, b); err != nil {
		return
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return
		} else {
			fmt.Println(bodyBytes)
		}
		return
	} else {
		defer r.Body.Close()
		if _, err := ioutil.ReadAll(r.Body); err != nil {
			return
		}
	}
}

func (c *WooClient) GetAllVariations(pageNr int, productID string) ([]Variation, error) {
	values := url.Values{}
	values.Add("per_page", "100")
	values.Add("page", strconv.Itoa(pageNr))

	if r, err := c.Client.Get(Products+productID+Variations, values); err != nil {
		if err == io.EOF {
			err = nil
		}
		return nil, err
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			if err == io.EOF {
				err = nil
			}
			return nil, err
		} else {
			fmt.Println(string(bodyBytes))
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
	} else {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			if err == io.EOF {
				err = nil
			}
			fmt.Print(string(bodyBytes))
			return nil, err
		} else {
			defer r.Body.Close()
			var finalRes []Variation
			var res []GetVariation
			err := json.Unmarshal(bodyBytes, &res)
			copier.Copy(&finalRes, &res)
			//fmt.Println(string(bodyBytes))
			if err != nil {
				return nil, err
			}

			return finalRes, nil
		}
	}
}

func (c *WooClient) BatchVariationOperation(operation string, products []Variation, toDelete []int, productID int) (*WooProductBatchObjResponse, error) {
	var batch WooVariationBatchObj
	switch operation {
	case Create:
		batch.Create = products
	case Update:
		batch.Update = products
	case Delete:
		batch.Delete = toDelete
	}

	b := &bytes.Buffer{}
	err := json.NewEncoder(b).Encode(batch)
	if err != nil {
		return nil, err
	}

	if r, err := c.Client.Post(Products+strconv.Itoa(productID)+Variations+"/"+Batch, nil, b); err != nil {
		if err == io.EOF {
			err = nil
		}
		return nil, err
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err == io.EOF {
			err = nil
		}
		defer r.Body.Close()
		if err != nil {
			return nil, err
		}

		if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
		if string(bodyBytes) == "[]" || string(bodyBytes) == "{}" || string(bodyBytes) == "" {
			return nil, nil
		}

		res := &WooProductBatchObjResponse{}
		if err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&res); err != nil {
			fmt.Print(string(bodyBytes))
			return nil, err
		}

		return res, nil
	}
}

func (c *WooClient) BatchVariationForStockOperation(operation string, products []VariationForStock, toDelete []int, productID int) (*WooProductBatchObjResponse, error) {
	var batch WooVariationForStockBatchObj
	switch operation {
	case Create:
		batch.Create = products
	case Update:
		batch.Update = products
	case Delete:
		batch.Delete = toDelete
	}

	b := &bytes.Buffer{}
	err := json.NewEncoder(b).Encode(batch)
	if err != nil {
		return nil, err
	}

	if r, err := c.Client.Post(Products+strconv.Itoa(productID)+Variations+"/"+Batch, nil, b); err != nil {
		if err == io.EOF {
			err = nil
		}
		return nil, err
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err == io.EOF {
			err = nil
		}
		defer r.Body.Close()
		if err != nil {
			return nil, err
		}

		if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
			fmt.Print(string(bodyBytes))
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
		if string(bodyBytes) == "[]" || string(bodyBytes) == "{}" || string(bodyBytes) == "" {
			return nil, nil
		}

		res := &WooProductBatchObjResponse{}
		if err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&res); err != nil {
			fmt.Print(string(bodyBytes))
			return nil, err
		}

		return res, nil
	}
}

func GetAllWoocommerceVariations(wooClient WooClient, productID string) ([]Variation, error) {
	var allWooVariations []Variation
	pageNr := 1
	for {
		wooVariations, err := wooClient.GetAllVariations(pageNr, productID)
		pageNr += 1
		if err != nil {
			return nil, err
		}

		if len(wooVariations) == 0 {
			break
		}

		allWooVariations = append(allWooVariations, wooVariations...)
	}

	return allWooVariations, nil
}
