package woo

const (
	Color            = "Color"
	Size             = "Size"
	Products         = "products/"
	PaymentGateways  = "payment_gateways"
	Attributes       = "attributes"
	Variations       = "/variations"
	Categories       = "categories"
	Batch            = "batch"
	Create           = "create"
	Update           = "update"
	Delete           = "delete"
	WooVariableType  = "variable"
	BrandAttribute   = "Brand"
	ProductID        = "productid"



)

