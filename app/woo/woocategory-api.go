package woo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

func (c *WooClient) GetAllCategories(pageNr int) ([]Category, error) {
	values := url.Values{}
	values.Add("per_page", "10")
	values.Add("page", strconv.Itoa(pageNr))

	if r, err := c.Client.Get(Products+Categories, values); err != nil {
		return nil, err
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return nil, err
		} else {
			fmt.Println(string(bodyBytes))
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
	} else {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return nil, err
		} else {
			var res []Category
			err := json.Unmarshal(bodyBytes, &res)
			if err != nil {
				return nil, err
			}

			return res, nil
		}
	}
}

func (c *WooClient) AddCategory(product Category) (*Category, error) {
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(product)
	a := b
	if r, err := c.Client.Post(Products+Categories, nil, b); err != nil {
		fmt.Print(a)
		return nil, err
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			fmt.Print(a)
			return nil, err
		} else {
			fmt.Println(string(bodyBytes))
			fmt.Print(a)
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return nil, err
		}

		res := &Category{}
		if err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&res); err != nil {
			fmt.Print(a)
			return nil, err
		}

		return res, nil
	}
}

func (c *WooClient) UpdateCategory(product Category) (*Category, error) {
	b := &bytes.Buffer{}
	json.NewEncoder(b).Encode(product)
	if r, err := c.Client.Put(Products+Categories+"/"+strconv.Itoa(product.Id), nil, b); err != nil {
		return nil, err
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return nil, err
		} else {
			fmt.Println(string(bodyBytes))
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return nil, err
		}

		res := &Category{}
		if err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&res); err != nil {
			return nil, err
		}

		return res, nil
	}
}

func (c *WooClient) DeleteCategory(categoryID string) {
	if r, err := c.Client.Delete(Products+Categories+"/"+categoryID, nil, nil); err != nil {
		return
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		return
	} else {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return
		} else {
			fmt.Println(string(bodyBytes))
		}
	}
}

func (c *WooClient) BatchCategoryOperation(operation string, categories []Category, toDelete []int) (*WooCategoryBatchObjResponse, error) {
	var batch WooCategoryBatchObj
	switch operation {
	case Create:
		batch.Create = categories
	case Update:
		batch.Update = categories
	case Delete:
		batch.Delete = toDelete
	}

	b := &bytes.Buffer{}
	err := json.NewEncoder(b).Encode(batch)
	if err != nil {
		return nil, NewWooError("500", err.Error())
	}

	if r, err := c.Client.Post(Products+Categories+"/"+Batch, nil, b); err != nil {
		return nil, err
	} else if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		if bodyBytes, err := ioutil.ReadAll(r.Body); err != nil {
			return nil, err
		} else {
			fmt.Println(string(bodyBytes))
			return nil, NewWooError(strconv.Itoa(r.StatusCode), string(bodyBytes))
		}
	} else {
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return nil, err
		}

		res := &WooCategoryBatchObjResponse{}
		if err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&res); err != nil {
			return nil, err
		}
		return res, nil
	}
}
