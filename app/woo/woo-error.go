package woo

import "fmt"

type WooError struct {
	error
	Status  string
	Message string
}

func (e *WooError) Error() string {
	return fmt.Sprintf("Woocommerce API: %s status: %s", e.Message, e.Status)
}

func NewWooError(status string, msg string) *WooError {
	return &WooError{Status: status, Message: msg}
}
