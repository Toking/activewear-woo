package net

import (
	"activewear-woo/pkg/woocom-lib/request"
)

// URLBuilder interface
type URLBuilder interface {
	GetURL(req request.Request) string
}
