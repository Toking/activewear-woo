package request

import (
	"bytes"
	"io"
	"net/url"
)

// Request ...
type Request struct {
	Method   string
	Endpoint string
	Values   url.Values
	Reader   io.Reader
}

type RequestWithBody struct {
	Method   string
	Endpoint string
	Bytes   *bytes.Buffer
}
