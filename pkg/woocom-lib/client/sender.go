package client

import (
	"activewear-woo/pkg/woocom-lib/request"
	"net/http"
)

// Sender interface
type Sender interface {
	Send(req request.Request) (resp *http.Response, err error)
}
