package client

import (
	"activewear-woo/pkg/woocom-lib/auth"
	"activewear-woo/pkg/woocom-lib/net"
	"activewear-woo/pkg/woocom-lib/options"
	"activewear-woo/pkg/woocom-lib/url"
	go_net "net"
	"net/http"
	"time"
)

const (
	timeout               = 20 * time.Minute
	keepAlive             = 20 * time.Minute
	tlsHandshakeTimeout   = 20 * time.Minute
	expectContinueTimeout = 20 * time.Minute
	responseHeaderTimeout = 20 * time.Minute
	httpClientTimeout     = 20 * time.Minute
	maxIdleConns          = 2000
	maxConnsPerHost       = 2000
)

// Factory Structure
type Factory struct {
}

// NewClient method creates new Client
func (f *Factory) NewClient(o options.Basic) Client {
	authenticator := f.NewAuthenticator(o)

	urlBuilder := url.Builder{}
	urlBuilder.SetOptions(o)
	urlBuilder.SetQueryEnricher(authenticator)

	sender := f.NewSender(urlBuilder, o)
	c := Client{
		sender: &sender,
	}
	return c
}

//New HTTPClient ...
func New() *http.Client {
	tr := &http.Transport{
		DialContext: (&go_net.Dialer{
			Timeout:   timeout,
			KeepAlive: keepAlive,
		}).DialContext,
		TLSHandshakeTimeout: tlsHandshakeTimeout,

		ExpectContinueTimeout: expectContinueTimeout,
		ResponseHeaderTimeout: responseHeaderTimeout,

		MaxIdleConns:    maxIdleConns,
		MaxConnsPerHost: maxConnsPerHost,
	}
	cli := &http.Client{
		Transport: tr,
		Timeout:   httpClientTimeout,
	}

	return cli
}

// NewSender method creates new Sender
func (f *Factory) NewSender(u url.Builder, o options.Basic) net.Sender {
	httpClient := New()
	requestCreator := f.NewRequestCreator()
	requestEnricher := f.NewAuthenticator(o)

	sender := net.Sender{}
	sender.SetURLBuilder(&u)
	sender.SetHTTPClient(httpClient)
	sender.SetRequestCreator(&requestCreator)
	sender.SetRequestEnricher(requestEnricher)
	return sender
}

// NewRequestCreator ...
func (f *Factory) NewRequestCreator() net.HTTP {
	return net.HTTP{}
}

// NewAuthenticator ...
func (f *Factory) NewAuthenticator(o options.Basic) *auth.Authenticator {
	oauth := auth.OAuth{}
	oauth.SetMicrotimer(&auth.MicroTimer{})

	ba := auth.BasicAuthentication{}

	authenticator := auth.Authenticator{}
	authenticator.SetOAuth(oauth)
	authenticator.SetBasicAuth(ba)
	authenticator.SetOptions(o)

	return &authenticator
}
